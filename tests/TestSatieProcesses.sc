TestSatieProcesses : SatieUnitTest {

	var server, satie, environment;

	setUp {
		server = Server(this.class.name);
		satie = Satie(SatieConfiguration(server));

		this.boot(satie);

		// make a dummy Process
		environment = Environment.make { |self|
			~setup = { |self, name, group|
				self.name = name;
				self.group = group;
				self.foobar = \foobar;
			};
		};

		satie.makeProcess(\dummy, environment);
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_boot_makeProcess {
		satie.config.processes.keys.do { |key|
			this.assertEquals(
				satie.processes[key].notNil,
				true,
				"Satie called makeProcess for Process \'%\'".format(key)
			)
		}
	}

	test_makeProcess {
		this.assertEquals(
			satie.processes[\dummy].notNil,
			true,
			"makeProcess should have created a process named 'dummy' in processes dictionary"
		);
	}

	test_process_satieInstance {
		var process = satie.makeProcessInstance(\foobar, \dummy);

		this.assertEquals(
			process.satieInstance,
			satie,
			"A process should contain a satieInstance property that references its Satie instance"
		);
	}

	test_process_name {
		var process = satie.makeProcessInstance(\foobar, \dummy);

		this.assertEquals(
			process.name,
			\foobar,
			"A process should contain a name property equal to the process's name"
		);
	}

	test_process_group {
		var process = satie.makeProcessInstance(\foobar, \dummy);

		this.assertEquals(
			process.group,
			\foobar_group,
			"A process should contain a group property equal to the process's group name"
		);
	}

	test_makeProcessInstance {
		var process = satie.makeProcessInstance(\foobar, \dummy);

		this.assertEquals(
			process.notNil,
			true,
			"makeProcessInstance should have created process instance"
		);
	}

	test_makeProcessInstance_arglist_is_even {
		var results;
		var process = satie.makeProcessInstance(\foobar, \dummy, [\freq, 440.0, \amp, 0.1]);

		this.assertEquals(
			process.notNil,
			true,
			"makeProcessInstance should have created process if arglist was even numbered in size"
		);
		results = [process.freq, process.amp];
		this.assertEquals(
			results,
			[440.0, 0.1],
			"makeProcessInstance should have set the process properties from arglist"
		 );
	}

	test_makeProcessInstance_arglist_not_even {
		var process = satie.makeProcessInstance(\foobar, \dummy, [\a, 1, \b, 2, "whoops"]);

		this.assertEquals(
			process,
			nil,
			"makeProcessInstance should not have created process if arglist was odd numbered in size"
		);
	}

	test_makeProcessInstance_arglist_bufnum {
		var process;

		satie.loadSample('mySample', Platform.resourceDir +/+ "sounds/a11wlk01.wav");
		server.sync;

		process = satie.makeProcessInstance(\foobar, \dummy, [\bufnum, \mySample]);

		this.assertEquals(
			process.bufnum,
			satie.audioSamples[\mySample].bufnum,
			"makeProcessInstance Buffer name should have been replaced with actual Buffer number in args"
		);
	}

	test_makeProcessInstance_setup {
		var process = satie.makeProcessInstance(\foobar, \dummy);

		this.assertEquals(
			process.foobar,
			\foobar,
			"makeProcessInstance should have called the process' setup function"
		);
	}

	test_propertyProcHandler_with_property_function {
		var process = satie.makeProcessInstance(\foobar, \dummy);
		var group = satie.groups[process.group];

		// inject a property function into process
		process.put(\property, { |self, key, val| self[key] = val });
		// call SatieOSC.propertyProcHandler directly using a mock OSC message
		satie.osc.propertyProcHandler.value(['/satie/process/property', 'foobar', 'baz', 123, 'bar', 321]);

		this.assertEquals(
			[process.baz, process.bar],
			[123, 321],
			"SatieOSC.propertyProcHandler should have used the property function to set properties"
		);
	}

	test_propertyProcHandler_without_property_function {
		var process = satie.makeProcessInstance(\foobar, \dummy);
		var group = satie.groups[process.group];

		//  make sure no property function exists
		process.property = nil;
		// call SatieOSC.propertyProcHandler directly using a mock OSC message
		satie.osc.propertyProcHandler.value(['/satie/process/property', 'foobar', 'baz', 123, 'bar', 321]);

		this.assertEquals(
			process.baz,
			nil,
			"SatieOSC.propertyProcHandler without a property function should not have set any properties"
		);
	}

	test_propertyProcHandler_skip_key_is_function {
		var process = satie.makeProcessInstance(\foobar, \dummy);
		var group = satie.groups[process.group];

		// add a function to the process that sets some specific property
		process.put(\baz, { |self, val| self.lolz = val });
		// inject a property function into process
		process.put(\property, { |self, key, val| self[key] = val });
		// call SatieOSC.propertyProcHandler directly using a mock OSC message
		satie.osc.propertyProcHandler.value(['/satie/process/property', 'foobar', 'baz', 123, 'bar', 321]);

		this.assertEquals(
			[process.lolz, process.bar],
			[nil, 321],
			"SatieOSC.propertyProcHandler should have skipped setting a property that is a function"
		);
	}

	test_processSet_with_set_function {
		var process = satie.makeProcessInstance(\foobar, \dummy);
		var group = satie.groups[process.group];

		// inject a set unction into the process
		process.put(\set, { |self, key, val| self[key] = val });
		// call SatieOSC.processSet directly
		// try and set a property that doesn't exist yet so we know set did its job
		satie.osc.processSet(process, group, [\baz, 123, \bar, 321]);

		this.assertEquals(
			[process.baz, process.bar],
			[123, 321],
			"SatieOSC.processSet should have used the set function to set properties"
		);
	}

	test_processSet_put_property {
		var process = satie.makeProcessInstance(\foobar, \dummy);
		var group = satie.groups[process.group];

		// inject a couple of properties into the process
		process.put(\baz, 0);
		process.put(\bar, 0);
		// call SatieOSC.processSet directly
		satie.osc.processSet(process, group, [\baz, 123, \bar, 321]);

		this.assertEquals(
			[process.baz, process.bar],
			[123, 321],
			"SatieOSC.processSet should have called process.put() when no set function was found"
		);
	}

	test_processSet_set_group {
		var results = Array.new(2);
		var process = satie.makeProcessInstance(\foobar, \dummy);
		var group = satie.groups[process.group];

		// inject a synth in the group
		var synth = Synth(\testtone, [\gainDB, -60, \aziDeg, 45], group);

		// call SatieOSC.processSet directly
		// the process does not have a gainDB or aziDeg property so its group will be set instead
		satie.osc.processSet(process, group, [\gainDB, -30, \aziDeg, 15]);

		synth.get(\gainDB, { |val| results.add(val) });
		synth.get(\aziDeg, { |val| results.add(val) });
		server.sync;

		this.assertEquals(
			results,
			[-30.0, 15.0],
			"SatieOSC.processSet should have called process.put() when no set function was found"
		);
	}

	test_processSet_skip_key_is_function {
		var process = satie.makeProcessInstance(\foobar, \dummy);
		var group = satie.groups[process.group];

		// add a function to the process that sets some specific property
		process.put(\baz, { |self, val| self.lolz = val });
		// inject a property function into process
		process.put(\property, { |self, key, val| self[key] = val });
		// call SatieOSC.propertyProcHandler directly using a mock OSC message
		satie.osc.propertyProcHandler.value(['/satie/process/property', 'foobar', 'baz', 123, 'bar', 321]);

		this.assertEquals(
			[process.lolz, process.bar],
			[nil, 321],
			"SatieOSC.propertyProcHandler should have skipped setting a property that is a function"
		);
	}
}

