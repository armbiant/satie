// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/

~name = \misDroneADSR;
~description = "A rich drone sound with controllable attack and infinite sustain";
~channelLayout = \mono;

~function = { | freq = 200, attack = 100, dec = 0.3, sus = 0.5, rel = 6, amp = 0.75, gate = 1, win = \sin|
	var bnq = Array.with(
		1-SinOsc.ar(freq, mul: 0.7),
		1-SinOsc.ar((freq * 4) * 1.0533610359546 * (SinOsc.kr(0.01, mul: 0.01)+ 1), mul: 0.6),
		1-SinOsc.ar((freq * 5)  * 1.2541124095491 * (SinOsc.kr(0.01, mul:  0.01) + 1), mul: 0.5),
		1-SinOsc.ar((freq * 6) * 1.8768759933376 *(SinOsc.kr(0.01)-1), mul: 0.02);
	);
	var mix, env, envGen, resonance, verb;
	mix = bnq[0] + bnq[1] + bnq[2] + bnq[3] * 0.01;
	env = Env.adsr(attack, dec, sus, rel);
	envGen = EnvGen.kr(env, gate);
	resonance = DynKlank.ar(`[
		[
			LFNoise1.kr(freq*0.01).range(517, 521),
			LFNoise1.kr(freq*0.01).range(727, 732),
			LFNoise1.kr(freq*0.01).range(929, 934),
			LFNoise1.kr(freq*0.01).range(1045, 1050),
			LFNoise1.kr(freq*0.01).range(1127, 1132),
			LFNoise1.kr(freq*0.01).range(1168, 1179),
			LFNoise1.kr(freq*0.01).range(1135, 1140),
			LFNoise1.kr(freq*0.01).range(1390, 1405),
			LFNoise1.kr(freq*0.01).range(1567, 1573),
		],
		[
			LFNoise1.kr(1).range(-30, -20).dbamp,
			LFNoise1.kr(1).range(-35, -25).dbamp,
			LFNoise1.kr(1).range(-35, -25).dbamp,
			LFNoise1.kr(1).range(-35, -25).dbamp,
			LFNoise1.kr(1).range(-35, -25).dbamp,
			LFNoise1.kr(1).range(-30, -25).dbamp,
			-20.dbamp;
			-10.dbamp;
			-25.dbamp;
		],// amps
		[
			0.8,
			0.8,
			0.4,
			0.3,
			0.4,
			0.4,
			0.8,
			0.8,
			0.5
		]   // ring times
	],
	mix);
	verb = FreeVerb.ar(resonance, 0.7, 1, 0.5, Line.kr(0, 0.6, 0.5, mul: 0.7));
	verb = LeakDC.ar(verb);
	verb = verb * envGen * amp;
	Limiter.ar(verb, 0.7);
};

