// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

SatiePlugins : Dictionary {

	*newAudioPlugin { |path|
		^super.new.audioPluginsPath(path);
	}

	*newSpatPlugin { |path|
		^super.new.spatializerPath(path);
	}

	*newProcess { |path|
		^super.new.processPath(path);
	}

	/*
	*  Append plugins from path
	*/

	audioPluginsPath { |path|
		path.pathMatch.do{ |item|
			item.loadPaths;
			this.add(~name.asSymbol -> SatiePlugin.new(~name, ~description, ~function, ~channelLayout, ~setup));
			// reset global variables
			~name = ~description = ~function = ~channelLayout = ~setup = nil;
		};
	}

	spatializerPath { |path|
		path.pathMatch.do{ |item|
			item.loadPaths;
			this.add(~name.asSymbol -> SpatializerPlugin.new(~name, ~description, ~function, ~channelLayout, ~setup, ~numChannels, ~angles));
			// reset global variables
			~name = ~description = ~function = ~channelLayout = ~setup = ~numChannels = ~angles = nil;
		};
	}

	processPath { |path|
		path.pathMatch.do { |item|
			var filename = item.basename.splitext[0].asSymbol;
			var env = Environment.use { item.load };
			this.add(filename -> env);
		};
	}

	addAudioPlugin { |env|
		var name, description, function, channelLayout, setup;
		name = env[\name];
		description = env[\description];
		function = env[\function];
		channelLayout = env[\channelLayout];
		setup = env[\setup];
		this.add(name.asSymbol -> SatiePlugin.new(name.asSymbol, description, function, channelLayout, setup));
	}

	showPlugins {
		// post the key and associated plugin description
		"\n- Showing plugins for %".format(this.inspect).postln;
		this.keysValuesDo{|key, value| "% -> %".format(key, value.description.asString.quote).postln;};
		"----".postln;
	}
}
