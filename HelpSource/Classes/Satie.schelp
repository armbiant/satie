TITLE:: Satie
summary:: The main SATIE class
categories:: Libraries
related:: Overview/SATIE-Overview, Classes/SatieConfiguration, Classes/SatiePlugin, Classes/SatieFactory, Classes/SatieIntrospection

DESCRIPTION::
This is the main entry class into the SATIE audio rendering system. It relies on  LINK::Classes/SatieConfiguration::


CLASSMETHODS::

METHOD:: new
Creates a new Satie renderer

ARGUMENT:: satieConfiguration
An instance of LINK::Classes/SatieConfiguration::. Leaving this argument code::Nil:: will create a configuration object interally by calling LINK::Classes/SatieConfiguration#*new::.

ARGUMENT:: execFile
Optional. (LINK::Classes/String::): path to a TELETYPE::.scd:: file to be executed immediately after SATIE has booted.

INSTANCEMETHODS::

METHOD:: status
Returns SATIE's current status as a TELETYPE::Symbol::. There are 4 possible statuses:

DEFINITIONLIST::
    ## \initialized
    || SATIE's initial status after creation. Status returns to code::\initialized:: after SATIE has quit.
    ## \booting
    || SATIE's status when booting is in progress.
    ## \running
    || SATIE's status when booting has completed and SATIE is ready to be used.
    ## \quitting
    || SATIE's status when quitting is in progress.
::

METHOD:: config
The LINK::Classes/SatieConfiguration:: object used by an instance of Satie. Allows access to settings and can be used to alter configurations prior to LINK::#-boot::.

METHOD:: boot
The primary method for starting SATIE. Calls link::Classes/Server#-boot:: on the server and initializes SATIE.

METHOD:: booted
Returns code::true:: if SATIE is booted and ready to be used, code::false:: otherwise.

METHOD:: waitForBoot
Evaluate code::onComplete:: as soon as SATIE is booted and ready. This method will wait for booting to finish, or boot SATIE then wait if booting is not already in progress. If SATIE is already booted, it will evaluate the code::onComplete:: function immediately. See link::Classes/Server#-waitForBoot::.

ARGUMENT:: onComplete
A function to be executed after SATIE has finished booting.

METHOD:: reboot
Quits and then immediately reboots SATIE. If SATIE was not booted, this method will call link::Classes/Satie#-boot::.

METHOD:: reconfigure
Configure SATIE to use a different link::Classes/SatieConfiguration::. If SATIE is booted, calling code::reconfigure:: will re-init SATIE using the new configuration and then reboot.

ARGUMENT:: configuration
An instance of link::Classes/SatieConfiguration:: that will be used as the new configuration.

METHOD:: reconfigureFromJsonFile
Similar to link::Classes/Satie#-reconfigure::, but instead of passing a link::Classes/SatieConfiguration:: directly, this method takes a path to a JSON file as argument.

ARGUMENT:: path
A path to a JSON file that contains a valid SATIE configuration. For an example, see link::Classes/SatieConfiguration#JSON Configuration::.

METHOD:: reconfigureFromJsonString
Similar to link::Classes/Satie#-reconfigure::, but instead of passing a link::Classes/SatieConfiguration:: directly, this method takes stringified JSON as an argument.

ARGUMENT:: string
Stringified JSON that contains a valid SATIE configuration. For an example, see link::Classes/SatieConfiguration#JSON Configuration::.

METHOD:: makeInstance
Instantiate a compiled synthdef (audio source or effect) and insert a reference into the groupInstances dictionary.

ARGUMENT:: name
Symbol: a unique name by which the audio source or effect will be recognizable by SATIE

ARGUMENT:: synthDefName
Symbol: the name of the synth definition (see link::Classes/Satie#-makeSynthDef:: below)

ARGUMENT:: group
Symbol: the name of the group to place the audio source or effect in.

ARGUMENT:: synthArgs
Array: Synth properties to be taken into account at instantiation time. All settable synth properties can be manipulated via the standard set message provided by link::Classes/Synth:: instance.

returns:: a link::Classes/Synth::

METHOD:: makeSourceInstance
A wrapper around link::Classes/Satie#-makeInstance:: to explicitly create an audio source

returns:: a link::Classes/Synth::

METHOD:: makeFxInstance
A wrapper around link::Classes/Satie#-makeInstance:: to explicitly create an audio effect

returns:: a link::Classes/Synth::

METHOD:: execFile
Holds a path to the file that is exectud upon SATIE boot.

METHOD:: replacePostProcessor
Replaces post-processor pipeline at the output of the spatializer.

ARGUMENT:: pipeline
a list of symbols representing the post-processor names. See link::Classes/SatiePlugins:: and link::Examples/Plugins:: for more information on SATIE plugins

ARGUMENT:: outputIndex
Output channel index from which this post-processor will take effect, default 0

ARGUMENT:: spatializerNumber
Index number of the spatializer to which the post-process will be attached. Default 0

ARGUMENT:: synthArgs
An array of arguments, if any

METHOD:: effects
a dictionary containing effects plugins definitions

returns:: link::Classes/SatiePlugins::

METHOD:: synthDescLib

returns:: a link::Classes/SynthDescLib:: containing the SynthDefs generated by Satie.

METHOD:: cloneProcess
Clone a process

ARGUMENT:: processName
The name of the process

returns:: a clone of the process

(see an example of processes in LINK::Examples/Processes::)

METHOD:: makeKamikaze
Instantiate a Synth that frees itself when it becomes silent. Makes use of link::Classes/DetectSilence::.

ARGUMENT:: synthDefName
Symbol: the name of the synth definition (see link::Classes/Satie#-makeSynthDef::)

ARGUMENT:: group
Symbol: the name of the group to place the audio source or effect in.

ARGUMENT:: synthArgs
Array: Synth properties to be taken into account at instantiation time. All settable synth properties can be manipulated via the standard set message provided by link::Classes/Synth:: instance.

returns:: a link::Classes/Synth::

METHOD:: makeSatieGroup
A SATIE group is a dictionary that holds references to instantiated audio objects. It is particularly useful for changing properties on many objects with one message.

ARGUMENT:: name
Symbol: the group's name

ARGUMENT:: addAction
defines where this group will placed within the chain  of existing groups.

returns:: link::Classes/ParGroup::

METHOD:: cleanInstance
Remove the synth instance (audio source or effect)

ARGUMENT:: name
Symbol: name of the instance

ARGUMENT:: group
Symbol: from which group

This method does not return.

METHOD:: spat
Instance variable that holds the name of the curently used spatializer.

returns:: Symbol

METHOD:: makeProcess
Creates a process. See link::Examples/Processes:: for an example about processes.

ARGUMENT:: processName
Name of process as Symbol. See link::Examples/Processes:: for an example about processes.

ARGUMENT:: env
String: the Environment source (see LINK::Examples/Processes::)

returns:: link::Classes/Environment::

METHOD:: processes
A dictionary of processes. Read only.

returns:: link::Classes/Dictionary::

METHOD:: groups
A dictionary of groups. Read only.

returns:: link::Classes/Dictionary::

METHOD:: groupInstances
A dictionary of instances, per group. Read only.

returns:: link::Classes/Dictionary::

METHOD:: loadSample
Loads an audio sample into a Buffer and stores it inside the link::Classes/Satie#-audioSamples:: dictionary.

ARGUMENT:: name
Symbol: the chosen name for this Buffer.

ARGUMENT:: path
String: the path of the audio sample file to be loaded.

discussion::
code::
~satie = Satie.new;
~satie.boot;

// load an audio sample by giving it a name and providing the path to the file
~satie.loadSample(\mySample, Platform.resourceDir +/+ "sounds/a11wlk01.wav");

// the Buffer can be found by name
~satie.audioSamples[\mySample].plot;

// as a convenience, the name of the Buffer can substitute the actual bufnum when creating synths
// this works only when the bufnum argument is called \bufnum
~satie.makeSourceInstance(\foobar, \sndBuffer, synthArgs: [\bufnum, \mySample, \gate, 1, \gainDB, -20]);

// this also works when creating synths via OSC
NetAddr("localhost", 18032).sendMsg('/satie/scene/createSource', 'foobaz', 'sndBuffer', 'default', 'bufnum', 'mySample', 'gate', 1, 'gainDB', -20);
::

METHOD:: audioSamples
A dictionary of audio sample Buffers. Audio samples loaded into Satie via link::Classes/Satie#-loadSample:: will be stored here.

METHOD:: killSatieGroup
Remove a SATIE group.

ARGUMENT:: name
Symbol: the name of the group.

METHOD:: pauseInstance
Stop processing an instance but do not destroy it (sends it release() message).

ARGUMENT:: name
Symbol: the name of the instance.

ARGUMENT:: group
Symbol: the name of the group.

This method does not return.

METHOD:: debug
Turn on debug posting for SATIE

METHOD:: enableHeartbeat
Enables SATIE's heartbeat message which indicates that SATIE is booted. The OSC message code::/satie.heartbeat:: is sent to SATIE's responder address twice a second. This message is triggered whenever SATIE's Server sends out a code::/status.reply:: message after SATIE has finished booting.

ARGUMENT:: bool
A Boolean value which turns this feature On/Off. SATIE defaults to having it turned Off.

METHOD:: makeSynthDef
Calls link::Classes/SatieFactory#makeSynthDef:: method and creates references to compiled SynthDefs for use with the configured spatializers.

ARGUMENT:: name
(Symbol) The SynthDef's unique name on the server

ARGUMENT:: srcName
(Symbol) The name of the source (see link::Examples/Plugins::)

ARGUMENT:: srcPreToBusses
(Array) Passed to pre-process busses (see link::Classes/SatieFactory::)

ARGUMENT:: srcPostToBusses
(Array) Passed to post-process busses (see link::Classes/SatieFactory::)

ARGUMENT:: srcPreMonitorFuncsArray
(Array) Analysis/Monitoring plugins to be wrapped in the SynthDef (see link::Classes/SatieFactory:: and link::Examples/Plugins::)

ARGUMENT:: spatSymbolArray
(Array) Passed to spatialiser array (see link::Classes/SatieFactory::)

ARGUMENT:: firstOutputIndexes
(Array) A list of output bus objects or integer channel indexes for each teletype::spatSymbolArray:: value (see link::Classes/SatieFactory::)

ARGUMENT:: paramsMapper
(Array) Parameter mapper plugins (see link::Classes/SatieFactory::)

ARGUMENT:: synthArgs
Custom synth arguments passed to link::Classes/SatieFactory::

METHOD:: makeAmbi
Calls link::Classes/SatieFactory#makeAmbi:: method and creates references to compiled SynthDefs for use with ambisonic spatialization methods.

ARGUMENT:: name
(Symbol) The SynthDef's unique name on the server

ARGUMENT:: srcName
(Symbol) The name of the source (see link::Examples/Plugins::)

ARGUMENT:: preBusArray
(Array) Passed to pre-process busses

ARGUMENT:: postBusArray
(Array) Passed to post-process busses

ARGUMENT:: srcPreMonitorFuncsArray
(Array) Analysis/Monitoring plugins to be wrapped in the SynthDef (see link::Examples/Plugins::)

ARGUMENT:: ambiOrder
(int) Ambisonic Order

ARGUMENT:: ambiEffectPipeline
(Array) A pipeline of ambisonic effects

ARGUMENT:: ambiBus
(Bus) An audio Bus on which to output the Ambisonic B-format signal. The Bus must have the correct number of channels for the specified code::ambiOrder::.

ARGUMENT:: paramsMapper
(Array) Parameter mapper plugins

ARGUMENT:: synthArgs
Custom synth arguments passed to link::Classes/SatieFactory::

METHOD:: makeAmbiFromMono
Calls link::Classes/SatieFactory#makeAmbiFromMono:: method and creates references to compiled SynthDefs for use with ambisonic spatialization methods.

ARGUMENT:: name
(Symbol) The SynthDef's unique name on the server

ARGUMENT:: srcName
(Symbol) The name of the source (see link::Examples/Plugins::)

ARGUMENT:: preBusArray
(Array) Passed to pre-process busses

ARGUMENT:: postBusArray
(Array) Passed to post-process busses

ARGUMENT:: srcPreMonitorFuncsArray
(Array) Analysis/Monitoring plugins to be wrapped in the SynthDef (see link::Examples/Plugins::)

ARGUMENT:: hoaEncoderType
(Symbol) The HOA encoder type to be used by this specific SynthDef. Can be either code::\wave:: or code::\harmonic::.
If this argument is left code::nil::, the encoder type that is set in Satie's configuration will be used.
See link::Classes/SatieConfiguration#-hoaEncoderType:: for more information.

ARGUMENT:: ambiOrder
(int) Ambisonic Order

ARGUMENT:: ambiEffectPipeline
(Array) A pipeline of ambisonic effects

ARGUMENT:: ambiBus
(Bus) An audio Bus on which to output the Ambisonic B-format signal. The Bus must have the correct number of channels for the specified code::ambiOrder::.

ARGUMENT:: paramsMapper
(Array) Parameter mapper plugins

ARGUMENT:: synthArgs
Custom synth arguments passed to link::Classes/SatieFactory::

METHOD:: removeProcess
Remove a process from the pipeline and clear its references.

ARGUMENT:: processName
(Symbol) The name (id) of the process

PRIVATE:: cmdPeriod, createDefaultGroups, cleanProcessInstance, execPostBootActions, namesIds

EXAMPLES::
Post-processor example
code::
(
// prepare the server
s = Server.supernova.local;
~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);
~satie = Satie.new(~satieConfiguration);
~satie.waitForBoot({
    // display some information
    s.meter;
    s.makeGui;
    s.plotTree;
});
)
// add post-process
~satie.replacePostProcessor([\limiter], 0, 0);
::
