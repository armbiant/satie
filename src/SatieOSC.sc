SatieOSC {

	var satie;
	var <rootURI;
	var <>oscServerPort;
	var <>oscClientPort;
	var <>oscClientIP = "localhost";

	var <oscServer;

	// client
	var <dynamicResponder = true;
	var returnAddress;

	// used for audio renderer control
	var volume;     // will point to server volume / mute control
	var outputDB = 0;    // current state of the supercollider server output
	var outputTrimDB = 0;

	// custom update message. These keys will be mached with sent values.
	var update_message_keys = #[\aziDeg, \eleDeg, \gainDB, \delayMs, \lpHz];
	var <>update_custom_keys;

	var <oscDefs;
	var <heartbeat = false;

	// TODO satieContext must be an array in order to duplicate message forwarding to sc server
	*new { | satieContext, rootPath = "/satie", serverPort = 18032, clientPort = 18060 |
		^super.newCopyArgs(satieContext, rootPath, serverPort, clientPort).initOSC;
	}

	initOSC {
		oscDefs = IdentityDictionary.new;
		returnAddress = NetAddr(this.oscClientIP, this.oscClientPort);
		volume = satie.config.server.volume;
		update_custom_keys = List.new();
		oscServer = NetAddr.new("localhost", oscServerPort);
		this.createBaseHandlers;
	}

	createBaseHandlers {
		// create OSC handlers that are usable after Satie.init
		this.newOSC(\satieConfigure, this.configureSatie, "/satie/configure");
		this.newOSC(\satieQuit, this.quitSatie, "/satie/quit");
		this.newOSC(\satieReboot, this.rebootSatie, "/satie/reboot");
		this.newOSC(\satieBoot, this.bootSatie, "/satie/boot");
		this.newOSC(\satieHeartbeat, this.enableHeartbeat, "/satie/heartbeat");
	}

	createHandlers {
		// post OSC info after boot
		" - satie: %\n - rootURI: %\n - port: %".format(satie, rootURI, oscServerPort).postln;
		" + %".format(satie.config.server).postln;

		// set up default groups
		if(satie.groups[\default].isNil) {
			postln("satieOSC.INIT:  \default group not defined on the server, creating \default group on head");
			this.createGroup(\default);
		};

		// set up defaultFx group
		if(satie.groups[\defaultFx].isNil) {
			postln("satieOSC.INIT:  \defaultFx group not defined on the server, creating \defaultFx group on tail");
			this.createGroup(\defaultFx,\addToTail);
		};

		// scene level handler
		this.newOSC(\satieSceneCreateSource, this.createSourceHandler, "/satie/scene/createSource");
		this.newOSC(\satieSceneCreateKamikaze, this.createKamikazeHandler, "/satie/scene/createKamikaze");
		this.newOSC(\satieSceneCreateEffect, this.createEffectHandler, "/satie/scene/createEffect");
		this.newOSC(\satieSceneCreateProcess, this.createProcessHandler, "/satie/scene/createProcess");
		this.newOSC(\satieSceneCreateSourceGroup, this.createSourceGroupHandler, "/satie/scene/createSourceGroup");
		this.newOSC(\satieSceneCreateEffectGroup, this.createEffectGroupHandler, "/satie/scene/createEffectGroup");
		this.newOSC(\satieSceneCreateProcessGroup, this.createProcessGroupHandler, "/satie/scene/createProcessGroup");

		this.newOSC(\satieSceneDeleteNode, this.deleteNodeHandler, "/satie/scene/deleteNode");
		this.newOSC(\satieDebugFlag, this.debugFlagHandler, "/satie/scene/debug");
		this.newOSC(\satieClearScene, this.clearSceneHandler, "/satie/scene/clear");
		this.newOSC(\satiePostProcessorSet, this.postProcessorSetSimpleHandler, "/satie/scene/postproc/set");
		this.newOSC(\satiePostProcessorSetArray, this.postProcessorSetArrayHandler, "/satie/scene/postproc/setarray");
		this.newOSC(\satiePostProcessorApply, this.postProcessorApplyHandler, "/satie/scene/postproc/apply");
		this.newOSC(\satieAmbiPostProcessorSet, this.ambiPostProcessorSetSimpleHandler, "/satie/scene/ambipostproc/set");
		this.newOSC(\satieAmbiPostProcessorSetArray, this.ambiPostProcessorSetArrayHandler, "/satie/scene/ambipostproc/setarray");
		this.newOSC(\satieAmbiPostProcessorApply, this.ambiPostProcessorApplyHandler, "/satie/scene/ambipostproc/apply");
		this.newOSC(\satiePostProcessorPipelineSetProperty, this.postProcessorPipelineSetProperties, "/satie/scene/postproc/prop/set");

		// node level handlers
		this.newOSC(\satieSrcState, this.stateSourceHandler, "/satie/source/state");
		this.newOSC(\satieGroupState, this.stateGroupHandler, "/satie/group/state");
		this.newOSC(\satieProcState, this.stateProcHandler, "/satie/process/state");
		this.newOSC(\satieSrcSet, this.setSrcHandler, "/satie/source/set");
		this.newOSC(\satieGroupSet, this.setGroupHandler, "/satie/group/set");
		this.newOSC(\satieProcSet, this.setProcHandler, "/satie/process/set");
		this.newOSC(\satieSrcUpdate, this.updateSrcHandler, "/satie/source/update");
		this.newOSC(\satieGroupUpdate, this.updateGroupHandler, "/satie/group/update");
		this.newOSC(\satieProcUpdate, this.updateProcHandler, "/satie/process/update");
		this.newOSC(\satieSrcSetVec, this.setVecSourceHandler, "/satie/source/setvec");
		this.newOSC(\satieGroupSetVec, this.setVecGroupHandler, "/satie/group/setvec");
		this.newOSC(\satieProcSetVec, this.setVecProcHandler, "/satie/process/setvec");

		this.newOSC(\satieSrcGet, this.getSrcHandler, "/satie/source/get");
		this.newOSC(\satieParSrcGet, this.getParSrcHandler, "/satie/source/parameters/get");

		// process only handlers
		this.newOSC(\satieProcProp, this.propertyProcHandler, "/satie/process/property");
		this.newOSC(\satieProcEval, this.evalFnProcHandler, "/satie/process/eval");

		// client
		this.newOSC(\audioplugins, this.getAudioPlugins, "/satie/plugins");
		this.newOSC(\pluginDetails, this.getPluginDetails, "/satie/plugindetails");

		this.newOSC(\responderAddress, this.responderAddress, "/satie/responder");
		this.newOSC(\responderAddressIp, this.responderAddressIp, "/satie/responder/ip");
		this.newOSC(\responderAddressport, this.responderAddressPort, "/satie/responder/port");

		this.newOSC(\satieLoadFile, this.loadFile, "/satie/load");
		this.newOSC(\satieLoadSample, this.loadSample, "/satie/loadSample");
		this.newOSC(\satieServerOption, this.getServerOption, "/satie/server/options/get");
		this.newOSC(\satieGetConfigure, this.getSatieConfiguration, "/satie/configuration/get");
		this.newOSC(\satieStatus, this.getSatieStatus, "/satie/status");

		this.newOSC(\satieRendererSetOrientationDeg, this.setOrientationDegHandler, "/satie/renderer/setOrientationDeg");
		this.newOSC(\satieRendererSetOutputDB, this.setOutputDBHandler, "/satie/renderer/setOutputDB");
		this.newOSC(\satieRendererSetOutputTrimDB, this.setOutputTrimDBHandler, "/satie/renderer/setOutputTrimDB");
		this.newOSC(\satieRendererSetOutputMute, this.setOutputMuteHandler, "/satie/renderer/setOutputMute");
		this.newOSC(\satieRendererSetOutputDim, this.setOutputDimHandler, "/satie/renderer/setOutputDim");
		this.newOSC(\satieRendererFreeSynths, this.freeSynthsHandler, "/satie/renderer/freeSynths");

		// This is for the exclusive use of SendTrig, which (invariably) sends a trigger message to '/tr' path.
		// We use OSCdef directly because currently newOSC custom method does not give us full control over
		// OSCdef instance.
		oscDefs.put(
			\satieTrigger,
			OSCdef(\satieTrigger, this.triggerHandler, "/tr", satie.config.server.addr);
		);
		// and another receiver for SendReply attached to the envelope follower
		oscDefs.put(
			\satieEnvelope,
			OSCdef(\satieEnvelope, this.envelopeHandler, "/analysis", satie.config.server.addr);
		);

		this.createStatusReplyHandler;
	}

	// this OSCdef responds to the Server's /status.reply heartbeat
	// Satie will send its own heartbeat message /satie.heartbeat
	createStatusReplyHandler {
		oscDefs.put(
			\serverStatusReply,
			OSCdef(\serverStatusReply, this.sendHeartbeat, "/status.reply", satie.config.server.addr, NetAddr.langPort)
		);

		// trigger an update
		this.heartbeat_(heartbeat);
	}

	heartbeat_ { |bool = false|
		var def = oscDefs[\serverStatusReply];

		heartbeat = switch(bool)
			{true} {true}
			{false};

		// return early if OSCdef not defined
		if(def.isNil) {
			// always return the receiver in setter methods
			^this;
		};

		if(heartbeat) {
			def.enable;
		} {
			def.disable;
		};
	}

	/*      create a new OSC definition*/
	newOSC { | id, cb, path = \default |
		var key = id.asSymbol;
		oscDefs.put(
			key,
			OSCdef(key, cb, path, recvPort: oscServer.port)
		);
	}

	deleteOSC { |id|
		var key = id.asSymbol;
		OSCdef(key).free;
		oscDefs.removeAt(key);
	}

	deleteAll {
		oscDefs.keysDo({ |key| OSCdef(key).free });
		oscDefs.clear;
	}

	free {
		this.deleteAll;
		oscServer.disconnect;
	}

	removeGroup { | groupName |
		if ( satie.groups.includesKey(groupName.asSymbol) ,
			{
				if (satie.debug, {postf("•satieOSC.removeGroup:  group node: % \n",  groupName);});
				satie.groups.removeAt(groupName.asSymbol);     // remove node from global dictionary
		});
	}

	deleteSource { | nodeName |
		satie.groupInstances.keys.do({|gr, idx|
			if ( satie.groupInstances[gr].includesKey(nodeName.asSymbol) ,
				{
					this.clearSourceNode(nodeName.asSymbol, gr.asSymbol);
				});
		});
	}

	clearSourceNode {  | nameSym, group = \default |
		var node = nameSym.asSymbol;
		// is this a process node?
		if ( satie.processInstances.includesKey(node),
			{
				satie.cleanProcessInstance(node);
			},
			// else  its just a regular source
			{
				if (satie.groupInstances[group].includesKey(node),
					{
						satie.cleanInstance(node, group.asSymbol);
					},
					{
						"%: node: % does not exist".format(this.class.getBackTrace, node).warn;
					}
				);
				if (satie.debug,
					{postf("•satieOSC.clearSourceNode: delete  node  % in group %\n", nameSym, group);});
		});
	}


	getUriType { | uriPath |
		var charIndex, uriName, type;

		type = "";

		// type://name (i.e. plugin://DustDust, file://<path>

		// check URI name to make sure its valid

		if (uriPath.asString.contains("://") == false,
			{
				if (uriPath.size > 0, {warn("~getUriType:  uri type format error: "++uriPath++" \n");});
			},

			// else  // path ok, proceed
			{
				charIndex = uriPath.asString.find("://");
				type = uriPath.asString.split($:)[0];
				~type = type;
				// uriName = temp.asString.replace("://", "");
		});
		^type.asSymbol;
	}

	createSourceNode { |sourceName, synthName, groupName = \default, synthArgs = #[]|

		if(satie.debug, {
			"→    %: sourceName: %,  synthName: %,  groupName: %".format(this.class.getBackTrace, sourceName, synthName, groupName).postln
		});

		satie.makeSourceInstance(sourceName, synthName, groupName, synthArgs);
		if (satie.debug)
		{postf(">>satieOSC.createSourceNode:  creating %:  uri: %  group: %\n", sourceName, synthName, groupName)};
	}

	createKamikazeNode{ |synthName, groupName = \default, synthArgs = #[]|

		if(satie.config.debug, {
			"→    %: synthName: %,  groupName: %".format(this.class.getBackTrace, synthName, groupName).postln
		});

		satie.makeKamikaze(synthName, groupName, synthArgs);
		if (satie.debug)
		{postf(">>satieOSC.createKamikazeNode:  creating synth: %  group: %\n", synthName, groupName)};
	}


	createEffectNode { |sourceName, synthName , groupName=\defaultFx, auxBusIndex = 0, synthArgs = #[]|
		var synth;

		if(satie.debug) {
			"→    %: sourceName: %,  synthName: %,  groupName: %,  auxBusIndex %, synthArgs %".format(this.class.getBackTrace, sourceName, synthName, groupName, auxBusIndex, synthArgs).postln
		};

		synth = satie.makeFxInstance(sourceName, synthName, groupName, [\in, satie.aux[auxBusIndex]] ++ synthArgs);
		postf("satieOSC.createEffectNode: creating effects node % of group %, with  synth:  % on bus %, synthArgs: %\n", sourceName, groupName, synthName, auxBusIndex, synthArgs);
	}


	createGroup { | groupName, position=\addToHead|

		var groupPos = \head;
		var addAction = \addToHead;

		if (position == \addToTail,
			{
				groupPos = \effect;
				addAction = \addToTail;
		});

		if (position == \effect,
		{
				groupPos = \effect;
				addAction = \addToEffects;
		});

		if (satie.groups.includesKey(groupName),
			{
				postf("satieOSC.createGroup:  GroupNode % exists, no action \n", groupName);
				satie.groups[groupName.asSymbol];  // return group
			},
			// else create new group node
			{
				var group;

				if (satie.groups[groupName.asSymbol].notNil, // group already exists in SATIE, no need to create it
					{
						group = satie.groups[groupName.asSymbol];
					},
					// else  group does not exist in SATIE,  create it
					{
						group = satie.makeSatieGroup(groupName.asSymbol, addAction);
					});

				postf(">>satieOSC.createGroup:  creating   %   groupType: %\n", groupName, groupPos);

				^group;  // returns groups
			}
		);
	}

	getUriName { | uriPath |
		var uriName, uriSynth;
		~uriPAth = uriPath;
		uriName = uriPath.asString.split($ );
		uriSynth = uriName[0].asString.split($/)[2];

		^uriSynth.asString;
	}

	// receives OSC messages that look like:   /satie/load filename
	loadFile {
		^{ | msg |
			"SatieOSC : satieFileLoader called".postln;
			if ( (msg.size < 2 ) ,
				{"SatieOSC : satieFileLoader:  message missing filepath".warn;},
				// else
				{
					var filepath = msg[1].asString.standardizePath;   // can handle '~'\
					satie.executeExternalFile(filepath);
			});
		}
	}

	// receives OSC messages that look like: /satie/loadSample name path
	loadSample {
		^{ |msg|
			if(msg.size < 3) {
				"SatieOSC.loadSample: insufficient number of arguments received: %".format(msg).warn;
			} {
				var name = msg[1];
				var path = msg[2].asString.standardizePath;
				satie.loadSample(name, path);
			}
		}
	}

	// receives OSC message: /satie/configure JSON_string
	configureSatie {
		^{ |msg|
			if(msg.size < 2) {
				"SatieOSC.configureSatie: insufficient number of arguments received: %".format(msg).warn;
			} {
				satie.reconfigureFromJsonString(msg[1].asString);
			}
		}
	}

	bootSatie {
		^{ satie.boot; }
	}

	quitSatie {
		^{ satie.quit; }
	}

	rebootSatie {
		^{ satie.reboot; }
	}

	enableHeartbeat {
		^{ |msg|
			if(msg.size < 2) {
				"SatieOSC.enableHeartbeat: insufficient number of arguments received: %".format(msg).warn;
			} {
				satie.enableHeartbeat(
					(msg[1].asInteger === 1)
				);
			}
		}
	}

	sendHeartbeat {
		^{
			if(satie.status === \running) {
				returnAddress.sendMsg("/satie.heartbeat");
			};
		}
	}

}
