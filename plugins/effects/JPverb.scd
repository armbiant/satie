// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/

~name = \JPverb;
~description = "A mono reverb";
~channelLayout = \mono;

~function = { |in, t60 = 1, damp = 0, size = 1,  earlyDiff = 0.707, modDepth = 0.1, modFreq = 2, low = 1, mid = 1, high = 1, lowcut = 500, highcut = 2000|

    var sig = JPverb.ar(
		in: In.ar(in),
        t60: t60,
        damp: damp,
		size: size,
		earlyDiff: earlyDiff,
		modDepth: modDepth,
		modFreq: modFreq,
		low: low,
		mid: mid,
		high: high,
		lowcut: lowcut,
		highcut: highcut
    );
	sig
};
