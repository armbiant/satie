**31_channel_VBAP_to_HOA.py** is a command line utility that can be used to convert audio recordings into Ambisonics using SATIE.

Currently, this utility is designed to convert 31 mono recordings into a single Ambisonic B-format soundfile. Each of the 31 recordings corresponds to a single channel of the dome-shaped speaker system of the [Satosphère](https://sat.qc.ca/fr/satosphere).

The `satieNRT.scd` SuperCollider script can be used on its own, or via Python using `31_channel_VBAP_to_HOA.py`. This script can serve as a basis for developping other NRT SuperCollider scripts that make use of SATIE.

#### Requirements:

* SoX 14.4.2

    https://sox.sourceforge.net

* pysox 1.3.7

    https://github.com/rabitt/pysox

    `$ pip3 install --user sox`

#### Usage

###### Command

    $ python3 31_channel_VBAP_to_HOA.py [path] [ambi_order]

For help, type:

    $ python3 31_channel_VBAP_to_HOA.py --help

###### Arguments

    path

        A path to a folder containing 31 mono soundfiles.

        The naming convention of these files should be `XX_Title.wav` where 'X' is the channel number with 0 padding.

            example: 01_myTitle.wav, 02_myTitle.wav, ... 31_myTitle.wav

    ambi_order

        A number between 1 and 5 that indicates the desired Ambisonic order of the resulting B-format soundfile.

#### Documentation

The exported B-format soundfile will be saved to a new folder called `export` created at the root of `path`. The soundfile will be named according to the specified `ambi_order`: `export_ambiOrder_[ambi_order].wav`.

The sample rate of the exported B-format soundfile will match the sample rate of the original audio recordings. The audio file format of the export will be W64 to allow for multi-gigabyte file sizes.
