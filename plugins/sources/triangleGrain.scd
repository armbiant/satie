// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/

~name = \triangleGrain;
~description = "A simple triangular envelope grain with position, rate, and duration parameters";
~channelLayout = \mono;

~function = { |bufnum=0, startPos=0, rate=1, dur=0.1, trimDB=0|
	var sig, env;

	startPos = startPos.clip(0.0, 1.0);

	env = EnvGen.ar(Env.triangle(dur), doneAction: Done.freeSelf);
	sig = PlayBuf.ar(1, bufnum, rate: BufRateScale.kr(bufnum) * rate, startPos: BufFrames.kr(bufnum) * startPos, loop: 1);
	sig * trimDB.dbamp;
};

