// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*  Spatializer plugin definition

	Each spatializer plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~numChannels: (int) number of channels
	~channelLayout: (symbol) layout for each channel, usually mono or ambi
	~angles: Speaker array angles, if using VBAP call VBAPSpeakerArray
	~function: the definition of the spatializer

	where function should use the following input arguments:

	in
	aziDeg +/- 180 degrees
	elevDeg +/- 90 degrees
	gainDB  decibels
	delaySec  seconds
	lpHz    hertz
	spread (range 0-1) default = 0.01
*/

~name = \dodecahedronVBAP;
~description = "Dodecahedron layout (3D)";
~numChannels = 12;
~channelLayout = \mono;


~angles = ~spk12 = VBAPSpeakerArray.new(3, // make array globally available after loading the spatializer
	[
	    [0.0, 60],
		[-120 , 60],
		[120 , 60],
		[60, 0.0],
		[0.0, -30],
		[-60, 0.0],
		[-120, -30],
		[180.0, 0.0],
		[120 , -30],
		[-60 , -60],
		[-180.0, -60],
		[60, -60]
	]
);

~function = { |in = 0, aziDeg = 0, eleDeg = 45, gainDB = -99, delayMs = 1, lpHz = 15000, hpHz = 5, spread = 0.01|

	var gain= gainDB.dbamp;        // convert gainDB to gainAMP
	var delay = delayMs * 0.001;   // convert to seconds
	var slewDelay = 0.3;           //  note: this needs to be improved ... smoother
	var slewGain = 0.1;
	var slewFilter = 0.3;
	var slewPanning = 0.03;
	var outsig;

	// limit cutoff freq range and smooth changes
	lpHz = lpHz.clip(5.0, 20000.0).lag3(slewFilter);
	hpHz = hpHz.clip(5.0, 20000.0).lag3(slewFilter);

	outsig = in * gain.lag(slewGain);
	outsig = DelayC.ar(
		outsig,
		maxdelaytime: 0.5,
		delaytime: delay.lag(slewDelay)
	);
	outsig = LPF.ar(outsig, lpHz);
	outsig = BHiPass.ar(outsig, hpHz);

	VBAP.ar(
		numChans: ~spk12.numSpeakers,
		in: outsig,
		bufnum: ~vbuf12.bufnum,
		azimuth: aziDeg.circleRamp(slewPanning),
		elevation: eleDeg.circleRamp(slewPanning),
		spread: spread * 100
	);

};


~setup = { |satieInstance|
	~vbuf12 = Buffer.loadCollection(satieInstance.config.server, ~spk12.getSetsAndMatrices);
};
