# SATIE Changelog

## [1.6.5] - 2022-10-07
### Additions

- Querying for plugins obtains list of all plugins

### Fixes

- Refactoring of viariables and functions names for clarity
- Update links after repo move
- Update contributors list

## [1.6.5] - 2022-09-13
### Additions

- Update OSC API documentation
- Setting OSC responder address and port separately

### Fixes

- Improve some introspection handling
- Some refactoring
- Upodate Quark URL to the new location

## [1.6.4] - 2022-07-11
### Additions
- Introspection of plugin/SynthDef running instances

### Fixes
- Typos and other minor fixes in SpaceDolph and Gravity SynthDefs
- Update OSC API documentation

## [1.6.3] - 2022-04-04
### Additions
- A few new _source_ plugins
- Installation instructions for Windows
- Documentation on using `Pbind` in SATIE processes
- Send the entire OSC API on request

### Fixes
- Ensure OSC port number is an Integer
- Various fixes to the documentation

## [1.6.2] - 2021-12-06
### Additions

- CI: Trigger deployment to documentation website
- Documentation: add Raspberry Pi installation instructions

## [1.6.1] - 2021-09-14
### New features
- Handle userPluginsDirPath configuration via JSON
- Send JSON with SatieConfiguration values upon OSC request
- Obtain the value of a ServerOptions instance variable via OSC
### Fixes
- The OSC API document has been reformated
- Installation instructions have been updated
- The documentation has been improved and typos fixed
### Deprecations
- Forcefully creating buses for each spatializer. The user/programmer has more flexibility now
- `/satie/pluginargs` OSC message handling

## [1.6.0] - 2021-02-22
### New features
- add an additional, user-configurable path for custom plugins to SatieConfiguration

### Fixes
- remove `serverOptions`

## [1.5.0] - 2021-02-08
### New features
- Reconfiguration of SATIE via loading JSON file (or sending JSON string via OSC)
- SATIE sends a heartbeat when asked

### Fixes
- Many fixes to documentation
- Fixes to tests
- CI fixes
- Various fixes to SATIE initialization

### OSC API
- New OSC messages:
  - `/satie/configure`
  - `/satie/reboot`
  - `/satie/heartbeat`

## [1.4.0] - 2020-07-06

### New features
- SATIE has a Code of Conduct
- Improved CONTRIBUTING.md and HOW_TO_RELEASE.md guides
- New boot method `Satie.waitForBoot`
- Querying SATIE's status is possible via `Satie.status`
- All of SATIE's SynthDefs get stored in `Satie.synthDescLib`
- Inspecting SATIE's SynthDefs is possible via `Satie.synthDescLib.browse`
- SATIE contains a dictionary of named audio samples accessible via `Satie.audioSamples`
- Audio samples can be referenced as `\bufnum, <sample-name-symbol>` inside `synthArgs` or `argList` arrays
- Processes are now loaded at boot like other types of plugins
- `Satie.makeProcessInstance` can take a variable lenght `argList` of key-value pairs
- `SatieConfiguration` can be `init` and `configured` in two separate steps if required
- DAC tester allows panning only a subselection of speaker channels
- CI tests SATIE using SuperCollider 3.11.0
- Improved UnitTest SATIE boot and quit
- New UnitTest Classes where added as well as many new test cases

### OSC API
- Much improved OSC API documentation
- New OSC message `/satie/loadSample`
- New OSC message `/satie/scene/createKamikaze`
- `/satie/scene/createSource` OSC messages can take variable length key-value pair Synth arguments
- `/satie/scene/createEffect` OSC messages can take variable length key-value pair Synth arguments
- `/satie/scene/createProcess` OSC messages can take variable length key-value pair process properties
- Outbound OSC `/analysis` and `/trigger` messages are appended with `/<instance-name>`

### Fixes
- Improved `Satie.boot` and `Satie.quit`
- `Satie.debug` is the main debug flag
- `SatieConfiguration.debug` turns on debugging for that Class only
- When no config provided, `Satie.new` creates itself a basic `SatieConfiguration`
- All `listeningFormats` output their signal to dedicated buses which are routed to a new `\satieOutput` Synth
- Plugin `spread` argument is a value between 0-1, where `0 == min spread`, `1 == max spread`
- `VBAP` azimuth and elevation consistently use `CircleRamp`
- `VBAP` runs at audio rate instead of at control rate with `Lag`
- Method arguments named `defaultArgs` renamed `synthArgs`
- Method arguments named `ambiBusIndex` renamed `ambiBus`
- Removed default value for `ambiBus`
- Fixed cases where Floats where used instead of Integers to set numbers of channels
- Fixed `aziDeg` panning in `\stereoPanner`
- Processes no longer make use of the global variable `~satie`
- `Satie.cleanSlate` no longer iterates over a collection while also modifying it
- `SatieConfiguration` no longer sets Server's output channel number to 0 in some cases
- Fixed `/satie/scene/clear` not clearing correctly
- Fixed `plugins/processes/grainProcess.scd`
- Removed unused `Satie.options`
- General Plugin fixes
- Fixed broken documentation examples
- Fixed NRT guide and example script
- `ci/install_quarks.scd` script takes a `path` argument for better versatility
- Removed no longer necessary "Additional ambisonic resources" section from INSTALL-SATIE.md


## [1.3.1] - 2020-01-29

### New features
- added new postprocessor `\sub`

### Fixes
- Fix error when sending a '/satie/plugins' OSC message while using Ambisonic postprocessors
- Stop DAC tester window from quitting ther server when closed in some use cases
- Fix `\dodecahedronVBAP` spatializer channel order
- Fix sample rate dependent calculations which could cause blowups
- `nfIndex` is clipped to 0-1 range
- `lpHz` and `hpHz` arguments are now clipped to 5-20kHz
- Use Lag3 to smooth `lpHz` and `hpHz` controls
- `hpHz` argument is consistently `5` by default
- `spread` argument is consistently `1` by default
- Fix gain being applied twice when encoding into HOA

## [1.3.0] - 2019-12-23

### New features
- New HOA binaural decoding
- New configurable `SatieConfiguration.hoaEncoderType`
    - `\wave` the usual B-format encoder
    - `\harmonic` cpu-efficient alternative B-format encoding
    - `\lebedev50` VBAP Lebedev50 grid to B-format encoding
- New 'Lebedev50VBAP` spatializer plugin
- New experimental `Lebedev50Binaural` post-processor
- New `dodecahedronVBAP` spatializer plugin
- New `Satie.makeAmbiFromMono` method
- All plugins now have their `setup` function evaluated
- Post-Processor properties can now be set via OSC
- NRT rendering Python and SClang scripts

### Improvements
- Improved DAC Tester GUI utility
- Fixed some source plugins that weren't actually monophonic
- Validate `orders` argument againsts configured ambiOrders
- Improved Test suite
- Removed `HOADecLebedev` based Binaural hoa plugins
- Refactor to use SC-HOA's new in-source ambisonic resources
- Fix incorrect azimuth and elevation radian convertion
- Fix bug that broke the Server's volume range
- Improved introspection for Ambisonic post-processors
- SATIE now frees its port when quitting
- OSC can now call `replacePostProcessor` and `replaceAmbiPostProcessor`


## [1.2.0] - 2019-01-23

### New features
- New `Satie.quit` method
- New `Satie.clearScene` method
- New `SatieQueryTree` class
- New convenience method `Satie.switchMake`
- Allow function to be called immediately after boot
- Make use of SuperCollider's CmdPeriod and ServerTree classes
- Project now uses Gitlab CI
- Many new unit tests

### Improvements
- Improved booting
- Allow freeing OSCdefs
- OSC API message 'update' now more flexible
- Renamed SATIE plugin folders
- Renamed some SATIE plugins
- All plugins make use of the `~channelLayout` field
- Throw error when `listeningFormat` and `outBusIndex` don't match
- Removed duplicate plugin dictionary variables from class `Satie`
- SATIE no longer forces `blockSize = 128` when doing ambisonics
- SATIE no longer depends on NodeWatcher quark
- Removed unused methods
- Revised documentation
- Cleaned up OSC API



## [1.1.1] - 2018-08-15

### Improvements
- A better Readme



## [1.1.0] - 2018-08-13

### New features
- Analysis/monitoring side-chain with example monitoring plugins
- _delay_ post-processor
- Execute a file upon SATIE server boot
- Enable/disable compilation of SATIE plugins at boot

### Improvements
- Updated OSC API documentation which is now rendered in html
- Added new audio generator
- Ability to load custom plugins from user-defined directory
- Revised documentation

### Bugfixes
- Proper handling of sceneClear
- Proper handling of freeing synths from IDE
- Fix to quark file

### Removals
- SAT specific plugins



## [1.0.1] - 2018-04-19

### New features
- Support for ambisonics via SC-HOA quark
- Several mappers for each spatialiser
- Far and near field mappers

### Improvements
- Updated documentation
- Various bugfixes
