TestOscCommunication : SatieUnitTest {

	var server, config, satie, addr, dummy, a11wlk01;

	setUp {
		server = Server(this.class.name);
		config = SatieConfiguration(server, ambiOrders: [3]);
		satie = Satie(config);

		this.boot(satie);

		addr = satie.osc.oscServer;
		dummy = Environment.make {
			~routine;
			~myProperty = 0;
			~set = { |self, prop, val| self[prop] = val };
			~property = { |self, prop, val| self[prop] = val };
			~setVec = { |self, prop, valArray| self[prop] = valArray };
			~setUpdate = { |self...args| self.myProperty = args };
			~myFunction = { |self, val| self.myProperty = val };
			~makeRoutine = { |self| self.routine = Routine { loop { 1.0.wait } } };
			~start = { |self|
				self.routine.reset;
				self.routine.play;
			};
			~setup = { |self|
				self.makeRoutine;
				self.start;
			};
			~cleanup = { |self| self.routine.stop };
		};
		satie.makeSynthDef(\limit, \Limiter, spatSymbolArray: config.listeningFormat);
		satie.makeInstance(\mySource, \pbell1);
		satie.makeProcess(\dummy, dummy);
		satie.makeProcessInstance(\myProcess, \dummy);
		server.sync;

		a11wlk01 = Platform.resourceDir +/+ "sounds/a11wlk01.wav";
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_responder_port {
		var result;
		var value = ["127.0.0.255", 18066];
		addr.sendMsg('/satie/responder', value[0], value[1]);
		server.sync;
		result = [satie.osc.oscClientIP, satie.osc.oscClientPort];
		this.assertEquals(result, value, "/satie/responder set the client's ip:port.");
	}

	test_satie_heartbeat {
		var count = 0;

		OSCdef(\testHeartbeat, { count = count + 1; }, '/satie.heartbeat', NetAddr.localAddr, satie.osc.oscClientPort);

		addr.sendMsg('/satie/heartbeat', 1);
		// wait a few seconds for heartbeat messages to be received
		2.wait;

		this.assertEquals(
			count > 0,
			true,
			"/satie/heartbeat 1 should have enabled heartbeat messages"
		);

		// turn off heartbeat
		addr.sendMsg('/satie/heartbeat', 0);
		// reset count
		count = 0;
		// wait a few seconds
		2.wait;

		this.assertEquals(
			count === 0,
			true,
			"/satie.heartbeat 0 should have disabled heartbeat messages"
		);

		// free the responder
		OSCdef(\testHeartbeat).free;
	}

	test_satie_heartbeat_default_disabled {
		var count = 0;

		OSCdef(\testHeartbeat, { count = count + 1; }, '/satie.heartbeat', NetAddr.localAddr, satie.osc.oscClientPort);
		2.wait;

		this.assertEquals(
			count === 0,
			true,
			"/satie.heartbeat messages should be disabled by default"
		);

		// free the responder
		OSCdef(\testHeartbeat).free;
	}

	test_loadFile {
		var path = this.class.filenameSymbol.asString.dirname +/+ "data" +/+ "loadFile.scd";
		~loaded = false;
		addr.sendMsg('/satie/load', path);
		this.wait({ ~loaded }, "/satie/load failed to load file", 1);
	}

	test_scene_clear {
		var results, osc;
		addr.sendMsg('/satie/scene/clear');
		server.sync;

		// check groups don't contain any nodes
		results = satie.groupInstances.collect { |dict| dict.isEmpty };
		this.assertEquals(
			results.asEvent,
			(default: true, defaultFx: true, postProc: true, ambiPostProc: true),
			"/satie/scene/clear cleared the scene."
		);
	}

	test_scene_debug {
		addr.sendMsg('/satie/scene/debug', 1);
		server.sync;
		this.assertEquals(satie.debug, true, "/satie/scene/debug set the debug flag");
	}

	test_scene_deleteNode {
		addr.sendMsg('/satie/scene/deleteNode', 'mySource');
		server.sync;
		this.assertEquals(
			satie.groupInstances[\default][\mySource].isNil,
			true,
			"/satie/scene/deleteNode deleted the specified node"
		);
	}

	test_scene_createSource {
		addr.sendMsg('/satie/scene/createSource', 'anotherSource', 'testtone');
		server.sync;
		this.assertEquals(
			satie.groupInstances[\default][\anotherSource].notNil,
			true,
			"/satie/scene/createSource created a new source node"
		);
	}

	// there are no Dictionaries that keep track of the 'kamikaze' synths
	// we have to inspect the /n_info reply from querying the synth's Group to test that it was created
	test_scene_createKamikaze {
		var nodeID;

		addr.sendMsg('/satie/scene/createKamikaze', 'testtone');
		server.sync;

		// Group.query will reply with /n_info and we can capture it using a function
		// /n_info messages for Groups end with: head nodeID, tail nodeID
		// Our Synth's nodeID is at index 6
		satie.groups[\default].query({ |...args| nodeID = args[6] });
		server.sync;

		// here we want to check that the head_nodeID isn't -1 (no node)
		this.assert(
			nodeID !== -1,
			"/satie/scene/createKamikaze created a kamikaze with nodeID: %".format(nodeID)
		);
	}

	test_scene_createKamikaze_synthArgs {
		var nodeID, responder, reply;
		responder = OSCFunc({ |msg| reply = msg; }, '/n_set', server.addr);

		addr.sendMsg('/satie/scene/createKamikaze', 'testtone', 'default', 'sfreq', 415);
		server.sync;

		// the ID of the node at the head of SATIE's default group
		// Our Synth's nodeID is at index 6
		satie.groups[\default].query({ |...args| nodeID = args[6] });
		server.sync;

		// will reply with a '/n_set' message
		server.sendMsg('/s_get', nodeID, 'sfreq');
		server.sync;

		this.assertEquals(
			reply,
			['/n_set', nodeID, 'sfreq', 415.0],
			"/satie/scene/createKamikaze was able to set its Synth's arguments"
		);
	}

	test_scene_createProcess {
		addr.sendMsg('/satie/scene/createProcess', 'anotherProcess', 'dummy');
		server.sync;
		this.assertEquals(
			satie.processInstances[\anotherProcess].notNil,
			true,
			"/satie/scene/createProcess created a new process"
		);
	}

	test_scene_createProcess_set_property {
		var value = 5;

		addr.sendMsg('/satie/scene/createProcess', 'anotherProcess', 'dummy', 'myProperty', value);
		server.sync;

		this.assertEquals(
			satie.processInstances[\anotherProcess].notNil,
			true,
			"/satie/scene/createProcess created a new process"
		);

		this.assertEquals(
			satie.processInstances[\anotherProcess].at(\myProperty),
			value,
			"/satie/scene/createProcess set the newly created process property"
		);
	}

	test_scene_createEffect {
		addr.sendMsg('/satie/scene/createEffect', 'myEffect', 'limit');
		server.sync;
		this.assertEquals(
			satie.groupInstances[\defaultFx][\myEffect].notNil,
			true,
			"/satie/scene/createEffect created an effects Synth node"
		);
	}

	test_scene_createEffect_synthArgs {
		var nodeID, responder, reply;
		responder = OSCFunc({ |msg| reply = msg; }, '/n_set', server.addr);

		addr.sendMsg('/satie/scene/createEffect', 'myEffect', 'limit', 'defaultFx', 0, 'level', 0.5);
		server.sync;

		// the ID of the node at the head of SATIE's defaultFx group
		// Our Synth's nodeID is at index 6
		satie.groups[\defaultFx].query({ |...args| nodeID = args[6] });
		server.sync;

		// will reply with a '/n_set' message
		server.sendMsg('/s_get', nodeID, 'level');
		server.sync;

		this.assertEquals(
			reply,
			['/n_set', nodeID, 'level', 0.5],
			"/satie/scene/createEffect was able to set its Synth's arguments"
		);
	}

	test_scene_createSourceGroup {
		addr.sendMsg('/satie/scene/createSourceGroup', 'mySourceGroup');
		server.sync;
		this.assertEquals(
			satie.groups[\mySourceGroup].notNil,
			true,
			"/satie/scene/createSourceGroup created a new source group"
		);
	}

	test_scene_createEffectGroup {
		addr.sendMsg('/satie/scene/createEffectGroup', 'myEffectGroup');
		server.sync;
		this.assertEquals(
			satie.groups[\myEffectGroup].notNil,
			true,
			"/satie/scene/createEffectGroup created a new effect group"
		);
	}

	test_scene_createProcessGroup {
		addr.sendMsg('/satie/scene/createProcessGroup', 'myProcessGroup');
		server.sync;
		this.assertEquals(
			satie.groups[\myProcessGroup].notNil,
			true,
			"/satie/scene/createProcessGroup created a new process group"
		);
	}

	test_nodeType_set {
		var result;
		var value = [-12.0, -6.0];
		[
			['/satie/source/set', 'mySource'],
			['/satie/group/set', 'default']
		].do { |msg|
			result = [];
			addr.sendMsg(msg[0], msg[1], 'gainDB', value[0], 'trimDB', value[1]);
			server.sync;
			['gainDB', 'trimDB'].do { |argument|
				satie.groupInstances[\default][\mySource].get(
					argument,
					{ |val|
						result = result.add(val)
					}
				)
			};
			server.sync;
			this.assertEquals(
				result,
				value,
				"/satie/<nodeType>/set set the control arguments for node: %.".format(msg[1])
			);
		}
	}

	test_nodeType_setvec {
		var result;
		var value = [62.0, 1.0, 0.5];
		[
			['/satie/source/setvec', 'mySource'],
			['/satie/group/setvec', 'default']
		].do { |msg|
			addr.sendMsg(msg[0], msg[1], 'note', value[0], value[1], value[2]);
			server.sync;
			satie.groupInstances[\default][\mySource].getn(index: \note, count: 3, action: { |val| result = val });
			server.sync;
			this.assertEquals(
				result,
				value,
				"/satie/<nodeType>/setvec set the arrayed control argument of node: %.".format(msg[1])
			);
		}
	}

	test_process_set {
		var process = satie.processInstances[\myProcess];
		addr.sendMsg('/satie/process/set', 'myProcess', 'five', 5, 'foo', 'bar');
		server.sync;
		this.assertEquals(
			[process[\five], process[\foo]],
			[5, 'bar'],
			"/satie/process/set called the process' set function"
		);
	}

	test_process_setvec {
		var array = [100.0, 200.0, 300.0];
		addr.sendMsg('/satie/process/setvec', 'myProcess', 'floats', array[0], array[1], array[2]);
		server.sync;
		this.assertEquals(
			satie.processInstances[\myProcess].at(\floats),
			array,
			"/satie/process/setvec called the process' setVec function."
		);
	}

	test_source_update {
		var result;
		var value = [90.0, 45.0, -50.0, 1.0, 15000];
		addr.sendMsg('/satie/source/update', 'mySource', value[0], value[1], value[2]);
		server.sync;
		satie.groupInstances[\default][\mySource].getn(index: 3, count: 5, action: { |val| result = val });
		server.sync;
		this.assertEquals(
			result,
			value,
			"/satie/source/update set all the node's update arguments."
		);
	}

	test_process_update {
		var result;
		var value = [90.0, 45.0, -50.0, 2000.0, 8000, []];
		addr.sendMsg('/satie/process/update', 'myProcess', value[0], value[1], value[2], value[3], value[4]);
		server.sync;
		result = satie.processInstances[\myProcess].at(\myProperty);
		this.assertEquals(
			result,
			value,
			"/satie/process/update called the process' setUpdate function."
		);
	}

	test_process_property {
		var value = 100;
		addr.sendMsg('/satie/process/property', 'myProcess', 'myProperty', value);
		server.sync;
		this.assertEquals(
			satie.processInstances[\myProcess].at(\myProperty),
			value,
			"/satie/process/property set the specified process property."
		);
	}

	test_process_eval {
		var value = [200, 300, 400];
		addr.sendMsg('/satie/process/eval', 'myProcess', 'myFunction', value[0], value[1], value[2]);
		server.sync;
		this.assertEquals(
			satie.processInstances[\myProcess].at(\myProperty),
			value,
			"/satie/process/eval evaluated the specified process function."
		);
	}

	test_introspection_plugins {
		var result;
		OSCFunc({ |msg| result = msg[0] }, '/plugins');
		addr.sendMsg('/satie/plugins');
		server.sync;
		this.assertEquals(
			result,
			'/plugins',
			"/satie/plugings introspection message received"
		);
	}

	test_introspection_plugindetails {
		var result;
		OSCFunc({ |msg| result = msg[0] }, '/arguments');
		addr.sendMsg('/satie/plugindetails', 'testtone');
		server.sync;
		this.assertEquals(
			result,
			'/arguments',
			"/satie/plugindetails introspection message received"
		);
	}

	test_postproc_set {
		var result;
		addr.sendMsg('/satie/scene/postproc/set', 'spatializerNumber', 1);
		addr.sendMsg('/satie/scene/postproc/set', 'outputIndex', 20);
		server.sync;
		result = [\spatializerNumber, \outputIndex].collect { |key| satie.postProcStruct[key] };
		this.assertEquals(
			result,
			[1, 20],
			"/satie/scene/postproc/set was able to set postProcStruct's spatializerNumber and outputIndex"
		);
	}

	test_postproc_setarray {
		var result;
		addr.sendMsg('/satie/scene/postproc/setarray', 'pipeline', 'envfol');
		addr.sendMsg('/satie/scene/postproc/setarray', 'synthArgs', 'amp', 5, 'lpf', 10);
		server.sync;
		result = [\pipeline, \synthArgs].collect { |key| satie.postProcStruct[key] };
		this.assertEquals(
			result,
			[[\envfol], [\amp, 5, \lpf, 10]],
			"/satie/scene/postproc/setarray was able to set postProcStruct's pipeline and synthArgs"
		);
	}

	test_postproc_apply {
		var result;
		addr.sendMsg('/satie/scene/postproc/setarray', 'pipeline' , 'limiter');
		addr.sendMsg('/satie/scene/postproc/setarray', 'synthArgs', 'limitDB', -20);

		addr.sendMsg('/satie/scene/postproc/apply');
		this.wait({ satie.postProcessors[\post_proc_0].notNil }, "Failed to create postProcessor Synth", maxTime: 3);

		satie.postProcessors[\post_proc_0].get(\limitDB, { |val| result = val });
		this.wait({ result.notNil }, "Failed to get Synth argument", maxTime: 3);

		this.assertEquals(
			result,
			-20,
			"/satie/scene/postproc/apply created new postProcessor with provided arguments"
		);
	}

	test_postproc_properties_set {
		var result;
		addr.sendMsg('/satie/scene/postproc/setarray', 'pipeline' , 'limiter');
		addr.sendMsg('/satie/scene/postproc/apply');
		this.wait({ satie.postProcessors[\post_proc_0].notNil }, "Failed to create postProcessor Synth", maxTime: 3);

		addr.sendMsg('/satie/scene/postproc/prop/set', 'post_proc_0', 'limitDB', -60);
		server.sync;

		satie.postProcessors[\post_proc_0].get(\limitDB, { |val| result = val });
		this.wait({ result.notNil }, "Failed to get Synth argument", maxTime: 3);

		this.assertEquals(
			result,
			-60,
			"/satie/scene/postproc/prop/set was able to set a running postProcessor's properties"
		);
	}

	test_ambipostproc_set {
		var result;
		addr.sendMsg('/satie/scene/ambipostproc/set', 'spatializerNumber', 1);
		addr.sendMsg('/satie/scene/ambipostproc/set', 'outputIndex', 20);
		addr.sendMsg('/satie/scene/ambipostproc/set', 'order', 3);
		server.sync;
		result = [\spatializerNumber, \outputIndex, \order].collect { |key| satie.ambiPostProcStruct[key] };
		this.assertEquals(
			result,
			[1, 20, 3],
			"/satie/scene/ambipostproc/set was able to set ambiPostProcStruct's spatializerNumber, outputIndex, and order"
		);
	}

	test_ambipostproc_setarray {
		var result;
		addr.sendMsg('/satie/scene/ambipostproc/setarray', 'pipeline', 'RotateAz');
		addr.sendMsg('/satie/scene/ambipostproc/setarray', 'synthArgs', 'rotateAziDeg', 90);
		server.sync;
		result = [\pipeline, \synthArgs].collect { |key| satie.ambiPostProcStruct[key] };
		this.assertEquals(
			result,
			[[\RotateAz], [\rotateAziDeg, 90]],
			"/satie/scene/ambipostproc/setarray was able to set ambiPostProcStruct's pipeline and synthArgs"
		);
	}

	test_ambipostproc_apply {
		var result;
		addr.sendMsg('/satie/scene/ambipostproc/setarray', 'pipeline' , 'RotateAz');
		addr.sendMsg('/satie/scene/ambipostproc/setarray', 'synthArgs', 'rotateAziDeg', 90);
		addr.sendMsg('/satie/scene/ambipostproc/set', 'order', 3);

		addr.sendMsg('/satie/scene/ambipostproc/apply');
		this.wait({ satie.ambiPostProcessors[\ambipost__s0_o3].notNil }, "Failed to create ambiPostProcessor Synth", maxTime: 3);

		satie.ambiPostProcessors[\ambipost__s0_o3].get(\rotateAziDeg, { |val| result = val });
		this.wait({ result.notNil }, "Failed to get Synth argument", maxTime: 3);

		this.assertEquals(
			result,
			90,
			"/satie/scene/ambipostproc/apply created new postProcessor with provided arguments"
		);
	}

	test_ambipostproc_properties_set {
		var result;
		addr.sendMsg('/satie/scene/ambipostproc/setarray', 'pipeline', 'RotateAz');
		addr.sendMsg('/satie/scene/ambipostproc/setarray', 'synthArgs', 'rotateAziDeg', 90);
		addr.sendMsg('/satie/scene/ambipostproc/set', 'order', 3);
		addr.sendMsg('/satie/scene/ambipostproc/apply');
		this.wait({ satie.ambiPostProcessors[\ambipost__s0_o3].notNil }, "Failed to create ambiPostProcessor Synth", maxTime: 3);

		addr.sendMsg('/satie/scene/postproc/prop/set', 'ambipost__s0_o3', 'rotateAziDeg', 56);
		server.sync;

		satie.ambiPostProcessors[\ambipost__s0_o3].get(\rotateAziDeg, { |val| result = val });
		this.wait({ result.notNil }, "Failed to get Synth argument", maxTime: 3);

		this.assertEquals(
			result,
			56,
			"/satie/scene/postproc/prop/set created new postProcessor with provided arguments"
		);
	}

	test_loadSample {
		var name = [\foo, \bar, \baz, \biz];

		// load some samples
		addr.sendMsg('/satie/loadSample', name[0], a11wlk01);
		addr.sendMsg('/satie/loadSample', name[1], a11wlk01);
		addr.sendMsg('/satie/loadSample', name[2], a11wlk01);
		addr.sendMsg('/satie/loadSample', name[3], a11wlk01);
		this.wait({ satie.audioSamples[name[3]].notNil }, "Failed to load audio sample Buffers", maxTime: 3);

		name.do { |name|
			this.assertEquals(
				satie.audioSamples[name].class,
				Buffer,
				"/satie/loadSample created a Buffer name % inside Satie.audioSamples dictionary"
			)
		}
	}

	test_bufnum_arg_createSource {
		var result;

		addr.sendMsg('/satie/loadSample', 'foo', a11wlk01);
		server.sync;

		addr.sendMsg('/satie/scene/createSource', 'foobar', 'sndBuffer', 'default', 'bufnum', 'foo');
		server.sync;

		satie.groupInstances[\default][\foobar].get(\bufnum, { |bufnum| result = bufnum });
		this.wait({ result.notNil }, "Failed to get Synth argument", maxTime: 3);

		this.assertEquals(
			result.asInteger,
			satie.audioSamples[\foo].bufnum,
			"/satie/scene/createSource Buffer name was replaced with bufnum in key value pair arguments"
		);
	}

	test_bufnum_arg_createKamikaze {
		var result, index;

		addr.sendMsg('/satie/loadSample', 'foo', a11wlk01);
		server.sync;

		addr.sendMsg('/satie/scene/createKamikaze', 'sndBuffer', 'default', 'bufnum', 'foo');
		server.sync;

		OSCFunc({ |msg| result = msg }, 'g_queryTree.reply', server.addr).oneShot;
		server.sendMsg('/g_queryTree', satie.groups[\default].nodeID, 1);
		this.wait({ result.notNil }, "Failed to get /g_queryTree reply", maxTime: 3);

		// get the value of bufnum in the reply message
		index = result.indexOf(\bufnum) + 1;
		result = result[index];

		this.assertEquals(
			result.asInteger,
			satie.audioSamples[\foo].bufnum,
			"/satie/scene/createKamikaze Buffer name was replaced with bufnum in key value pair arguments"
		);
	}

	test_bufnum_arg_createProcess {
		addr.sendMsg('/satie/loadSample', 'foo', a11wlk01);
		server.sync;

		addr.sendMsg('/satie/scene/createProcess', 'anotherProcess', 'dummy', 'bufnum', 'foo');
		server.sync;

		this.assertEquals(
			satie.processInstances[\anotherProcess].bufnum,
			satie.audioSamples[\foo].bufnum,
			"/satie/scene/createProcess Buffer name was replaced with bufnum in key value pair arguments"
		);
	}

	test_satieConfigure_json {
		var limit = 1000;
		var path, json, str;

		path = this.class.filenameSymbol.asString.dirname +/+ "data" +/+ "OSC.json";
		json = SatieJson.parseJsonFile(path);

		str = File.readAllString(path);
		addr.sendMsg('/satie/configure', str);

		// Satie reboot
		while { (satie.status !== \initialized) && (limit > 0) } {
			limit = limit - 1;
			0.02.wait;
		};
		while { (satie.status !== \running) && (limit > 0) } {
			limit = limit - 1;
			0.02.wait;
		};

		this.assertEquals(
			satie.status,
			\running,
			"/satie/configure Reboot after OSC configuration was successful"
		);

		this.assertEquals(
			satie.config.server.name,
			json.server.name,
			"/satie/configure applied setting 'name' to SATIE's server"
		);

		this.assert(
			Server.program == "exec scsynth",
			"/satie/configure applied setting 'supernova' to SATIE's server"
		);

		this.assertEquals(
			satie.config.minOutputBusChannels,
			json.minOutputBusChannels,
			"/satie/configure applied setting 'minOutputBusChannels' to SATIE's configuration"
		);

		this.assertEquals(
			satie.config.numAudioAux,
			json.numAudioAux,
			"/satie/configure applied setting 'numAudioAux' to SATIE's configuration"
		);

		this.assertEquals(
			satie.config.ambiOrders,
			json.ambiOrders,
			"/satie/configure applied setting 'ambiOrders' to SATIE's configuration"
		);

		this.assertEquals(
			satie.config.listeningFormat,
			json.listeningFormat,
			"/satie/configure applied setting 'listeningFormat' to SATIE's configuration"
		);

		this.assertEquals(
			satie.config.outBusIndex,
			json.outBusIndex,
			"/satie/configure applied setting 'outBusIndex' to SATIE's configuration"
		);

		// Cleanup:
		// remove the now unused Server stored in the server var
		server.remove;
		// assign the current Server to the server var
		server = satie.config.server;
		// now proceed to usual test tearDown
	}

	test_satie_quit_boot_reboot {
		var limit;

		// Satie quit OSC
		addr.sendMsg('/satie/quit');
		limit = 1000;
		while { (satie.status !== \initialized) && (limit > 0) } {
			limit = limit - 1;
			0.02.wait;
		};
		this.assertEquals(
			satie.status,
			\initialized,
			"/satie/quit successfully quit SATIE"
		);

		// since we aren't quitting using the special test quit method
		// we must wait some amount of time before sending the boot OSC message
		"%: Waiting before sending boot OSC message".format(thisMethod).warn;
		1.wait;

		// Satie boot OSC
		addr.sendMsg('/satie/boot');
		limit = 1000;
		while { (satie.status !== \running) && (limit > 0) } {
			limit = limit - 1;
			0.02.wait;
		};
		this.assertEquals(
			satie.status,
			\running,
			"/satie/boot successfully boot SATIE"
		);

		// Satie reboot
		addr.sendMsg('/satie/reboot');
		limit = 1000;
		while { (satie.status !== \initialized) && (limit > 0) } {
			limit = limit - 1;
			0.02.wait;
		};
		while { (satie.status !== \running) && (limit > 0) } {
			limit = limit - 1;
			0.02.wait;
		};
		this.assertEquals(
			satie.status,
			\running,
			"/satie/reboot successfully rebooted SATIE"
		);
	}

}
