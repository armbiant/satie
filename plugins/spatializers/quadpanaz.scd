// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Spatializer plugin definition

	Each spatializer plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~numChannels: (int) number of channels
	~channelLayout: (symbol) layout for each channel, usually mono or ambi
	~angles: Speaker array angles, if using VBAP call VBAPSpeakerArray
	~function: the definition of the spatializer

	where function should use the following input arguments:

	in
	aziDeg +/- 180 degrees
	elevDeg +/- 90 degrees
	gainDB  decibels
	delaySec  seconds
	lpHz    hertz
	spread (range 0-1) default = 0.01
*/


~name = \quadpanaz;
~description = "4 channel 2D layout (equal power on a circle)";
~numChannels = 4;
~channelLayout = \mono;
~angles = [ [-180.0, 45], [180.0, 45], [-1.0, 45], [1.0, 45] ];

~function = { |in = 0, aziDeg = 0, eleDeg = 45, gainDB = -99, delayMs = 1, lpHz = 15000, hpHz = 5, spread = 0.01|

	var gain = gainDB.dbamp;       // convert gainDB to gainAMP
	var delay = delayMs * 0.001;   // convert to seconds
	var slewDelay = 0.3;           // note: this needs to be improved ... smoother
	var slewGain = 0.1;
	var slewFilter = 0.3;
	var slewPanning = 0.030;
	var outsig;
	var max_ele = 30;
	var min_ele = -60;
	var azi;

	azi = aziDeg.linlin(-180.0, 180.0, -1.0, 1.0);

	// limit cutoff freq range and smooth changes
	lpHz = lpHz.clip(5.0, 20000.0).lag3(slewFilter);
	hpHz = hpHz.clip(5.0, 20000.0).lag3(slewFilter);

	outsig = in * gain.lag(slewGain);
	outsig = DelayC.ar(
		outsig,
		maxdelaytime: 0.5,
		delaytime: delay.lag(slewDelay)
	);
	outsig = LPF.ar(outsig, lpHz);
	outsig = BHiPass.ar(outsig, hpHz);

	PanAz.ar(numChans: 4, in: outsig, pos: azi);
};
