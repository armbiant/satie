// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin
*/

//SynthDef by Victor Comby

~name = \SpaceDolph;
~description = "Random pitched sine waves";
~channelLayout = \mono;
~function =  {
	|attack = 2, decay = 2, sustain = 1, release = 0, amp = 0.8|

	var sigroot, sigsub, sigmid, sighigh, sighigh2, lfosigmid, lfosighigh, lfolp, lfolp2, freqdetune, env, rez, sigfinal;

    lfosigmid = LFNoise1.ar(10).exprange(50, 300);
	lfosighigh = LFNoise1.ar(1).exprange(50, 600);
	rez = LFNoise1.ar(8).exprange(0.3, 1);
	lfolp = SinOsc.ar(
		LFNoise1.kr(5).exprange(0.5, 20)
	).exprange(10, 5000);
	lfolp2 = SinOsc.ar(
		LFNoise1.kr(5).exprange(0.5, 20)
	).exprange(500, 5000);
	env = EnvGen.ar(Env.adsr(attack, decay, sustain, release));
	freqdetune = SinOsc.ar(0.5).exprange(162, 166);
	sigroot = LFTri.ar(130.81/4) * env;
	sigsub = LFTri.ar(130.81/4, mul:0.05) * env;
	sigsub = BLowPass4.ar(sigsub, lfolp2, rez);
	sigmid = Pulse.ar(lfosighigh * 2) * env;
	sighigh = Saw.ar(lfosighigh * 20) * env;
	sighigh2 = Saw.ar(lfosighigh * 4) * env;
	sigfinal = sigroot + sigmid + sighigh + sighigh2;
	sigfinal = BLowPass4.ar(sigfinal, lfolp, rez);
	sigfinal = Limiter.ar(sigfinal, 0.4, 0.01);
	sigfinal = sigfinal + sigsub;
	sigfinal*amp
};


~setup = {}
