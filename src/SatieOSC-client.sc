+ SatieOSC {
	setResponderAddress {| addr|
		if(satie.debug, {"% addy: %".format(this.class.getBackTrace, addr).postln;});
		this.oscClientPort = addr.port;
		this.oscClientIP = addr.ip;
		returnAddress = NetAddr(this.oscClientIP, this.oscClientPort);
		if(satie.debug,
			{"% addy after: %, returnAddress: %".format(this.class.getBackTrace, this.oscClientPort, returnAddress).postln;}
		);
		dynamicResponder = false;
	}

	responderAddress {
		^{ | args, time, addr, recvPort |
			var ip, port;
			if (args.size == 3,
				{
					ip = args[1];
					port = args[2];
					this.setResponderAddress(NetAddr(ip.asString, port.asInteger));
				},
				{"% wrong number of arguments, should be 2, %".format(this.class.getBackTrace, args).postln;}
			)
		}
	}

	responderAddressIp {
		^{ | args, time, addr, recvPort |
			var ip;
			if (args.size == 2,
				{
					ip = args[1];
					this.setResponderAddress(NetAddr(ip.asString, this.oscClientPort));
				},
				{"% wrong number of arguments, should be 1, %".format(this.class.getBackTrace, args).postln;}
			)
		}
	}

	responderAddressPort {
		^{ | args, time, addr, recvPort |
			var port;
			if (args.size == 2,
				{
					port = args[1];
					this.setResponderAddress(NetAddr(this.oscClientIP, port.asInteger));
				},
				{"% wrong number of arguments, should be 1, %".format(this.class.getBackTrace, args).postln;}
			)
		}
	}

	getSatieConfiguration {
		^{ | args, time, addr, recvPort |
			var json;
			if(satie.debug, {"% arguments: %".format(this.class.getBackTrace, args).postln;});
			if (dynamicResponder,
				{
					this.setResponderAddress(addr);
				}
			);
			json = satie.inspector.currentConfigJSON();
			if(satie.debug, {"% json: %".format(this.class.getBackTrace, json).postln;});
			returnAddress.sendMsg("/satie/configuration", json);
		}
	}

	getSatieStatus {
		^{ | args, time, addr, recvPort |
			var answer;
			if (dynamicResponder,
				{
					this.setResponderAddress(addr);
				}
			);
			answer = satie.status();
			returnAddress.sendMsg("/satie/status", answer);
		}
	}

	getAudioPlugins {
		^{ | args, time, addr, recvPort |
			var json;
			if(satie.debug, {"% arguments: %".format(this.class.getBackTrace, args).postln;});
			if (dynamicResponder,
				{
					this.setResponderAddress(addr);
				}
			);
			json = satie.inspector.getPluginsSrcJSON();
			if(satie.debug, {"% json: %".format(this.class.getBackTrace, json).postln;});
			returnAddress.sendMsg("/plugins", json);
		}
	}

	getPluginDetails {
		^{| args, time, addr, recvPort |
			var pluginName, json;
			pluginName = args[1];
			if(satie.debug, {"% arguments: %".format(this.class.getBackTrace, args).postln;});
			if (dynamicResponder,
				{
					this.setResponderAddress(addr);
				}
			);
			json = satie.inspector.getSynthDefParametersJSON(pluginName);
			returnAddress.sendMsg("/arguments", json);
		}
	}

	triggerHandler {
		^{ |args, time, addr, recvPort|
			//  args = sendTrig message: [/tr, node-id, trigger-id, trigger-value]
			SatieQueryTree.get(
				server: satie.config.server,
				action: { |snapshot|
					snapshot.nodeIds.do({ |id|
						var instanceName;
						if(id == args[1]) {
							satie.namesIds.keysValuesDo({ |key, value|
								if (value == id) {
									instanceName = key;
								}
							});
							returnAddress.sendMsg("/trigger/"++instanceName, args[3]);
						}
					});
				}
			);
		}
	}

	envelopeHandler {
		^{ |args, time, addr, recvPort|
			SatieQueryTree.get(
				server: satie.config.server,
				action: { |snapshot|
					snapshot.nodeIds.do({ |id|
						var instanceName, restArgs;
						if(id == args[1]) {
							satie.namesIds.keysValuesDo({ |key, value|
								if (value == id) {
									instanceName = key;
								}
							});
							// because SendReply allows for a list of values to be sent...
							restArgs = args.copyRange(3, args.size() -1);
							returnAddress.sendMsg("/analysis/"++instanceName, *restArgs);
						}
					});
				}
			);
		}
	}
}
