TITLE:: SATIE Overview
summary:: Spatial Audio Toolkit for Immersive Environments
categories:: Libraries>SATIE

DESCRIPTION::
SATIE is an audio spatialization engine developed for realtime rendering of dense audio scenes to large multi-channel loudspeaker systems. It is a lower-level audio rendering process that maintains a dynamic DSP graph which is created and controlled via OSC messages from an external process. SATIE’s modular development environment provides for optimized real-time audio scene and resource management. There is no geometry per se in SATIE, rather, SATIE maintains a DSP graph of source nodes that are accumulated to a single "listener", corresponding to the renderer’s output configuration (stereo and/or multi-channel).

Its aim is to facilitate using 3D space in audio music/audio composition and authoring and to play well with 3D audio engines (so far it has been used with Blender and Unity3D) and could also serve as volumetric audio spatialization addition to more traditional desktop DAW systems.

For more information visit LINK::https://gitlab.com/sat-mtl/tools/satie/satie##SATIE:: website.


SECTION:: Basic workflow

SUBSECTION:: Configure and boot SATIE
First, we create a link::Classes/SatieConfiguration::, then, we create a link::Classes/Satie:: with this configuration and boot it.

SatieConfiguration needs to be given a emphasis::Server:: and at least one emphasis::listeningFormat::. Here, we give it the default Server and a stereo spatializer.

code::
~config = SatieConfiguration(s, [\stereoListener]);
::

SatieConfiguration should have set its server's number of output channels. emphasis::stereoListener:: has 2 output channels, therefore the server should have 2 outputs.

code::
~config.server.options.numOutputBusChannels
::

If needed, other server options can be set through SatieConfiguration.

code::
~config.server.options.numWireBufs = 2048;
~config.server.options.memSize = 1024 * 256;
::

SatieConfiguration scans SATIE's plugin folders and collects the plugins in its dictionaries.

code::
~config.sources;
~config.effects;
~config.spatializers;
~config.hoa;
~config.mappers;
~config.postprocessors;
~config.monitoring;
::

An instance of SATIE can be created using this configuration. During boot, SynthDefs for all of its plugins will be generated.
Once booted, SATIE will start listening to incoming OSC messages.

code::
~satie = Satie(~config);
~satie.boot;
::

SATIE can be stopped using the 'quit' method. This will clean up after itself.

code::
~satie.quit;
::

SECTION:: Use it
SATIE can be used either directly from SuperCollider or by sending it OSC messages from another application. See the examples in the following help files.

SUBSECTION:: Examples
The examples below show various facets of SATIE's functionality.
DEFINITIONLIST::
## LINK::Examples/SATIE-Basics::
|| Progressive examples of SATIE functionnality, how to use it from scide.
## LINK::Examples/Spatializers::
|| Overview of available spatializers
## LINK::Examples/Processes::
|| Example of SATIE processes
## LINK::Examples/Effects::
|| Introduction to using effects
## LINK::Examples/Plugins::
|| Description of SATIE plugins
## LINK::Examples/Ambi::
|| Ambisonic support in SATIE
## LINK::Examples/NRT::
|| NRT support in SATIE
::

SUBSECTION:: API
DEFINITIONLIST::
## LINK::Overview/OSC-API.html##SATIE OSC protocol (html)::
|| How to talk (and listen) to SATIE via OSC
::

SUBSECTION:: Classes
DEFINITIONLIST::
## LINK::Classes/Satie::
|| Main SATIE audio renderer class
## LINK::Classes/SatieConfiguration::
|| SATIE configuration class
## LINK::Classes/SatieFactory::
|| Satie Factory
##LINK::Classes/SatiePlugin::
|| Satie specific plugins
##LINK::Classes/SpatializerPlugin::
|| Spatiliser is a subclass of link::Classes/SatiePlugin::
##LINK::Classes/SatiePlugins::
|| A Dictionary of plugins
::

SUBSECTION:: Utilities
DEFINITIONLIST::
## TELETYPE::/SATIE/utils/dacTest.scd::
|| A GUI utility for testing speaker systems
## TELETYPE::/SATIE/utils/python/31_channel_VBAP_to_HOA/satieNRT.scd::
|| An example script for converting a VBAP recording to Ambisonic B-format using NRT rendering
::
